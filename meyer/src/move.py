#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
PI = 3.1415926535897

def move():
    # Starts a new node
    rospy.init_node('robot_cleaner', anonymous=True)
    velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    vel_msg = Twist()
    
    #Receiveing the user's input
    print("Ivan Meyer's node to draw a hexagon")
    speed = input("Input the lineal speed:")
    speed_2 = input("Input your speed (degrees/sec):")
    distance = 1
    
    speed = abs(speed)
    angular_speed = speed_2*2*PI/360

    # No direction
    vel_msg.linear.x = 0
    vel_msg.linear.y = 0
    vel_msg.linear.z = 0
    vel_msg.angular.x = 0
    vel_msg.angular.y = 0
    vel_msg.angular.z = 0

    for i in range(0, 6):
        if i == 0:
            # First traslation
            traslation(velocity_publisher, vel_msg, speed, distance)
        else:
            traslation(velocity_publisher, vel_msg, speed, 2*distance)

        rotation(velocity_publisher, vel_msg, angular_speed, 60)

    traslation(velocity_publisher, vel_msg, speed, distance)

    rospy.on_shutdown(finish)
    return

    
def traslation(velocity_publisher, vel_msg, speed, distance):
    vel_msg.linear.x = abs(speed)

    #Setting the current time for distance calculus
    t0 = rospy.Time.now().to_sec()
    current_distance = 0

    #Loop to move the turtle in an specified distance
    while(current_distance < distance):
        #Publish the velocity
        velocity_publisher.publish(vel_msg)
        #Takes actual time to velocity calculus
        t1=rospy.Time.now().to_sec()
        #Calculates distancePoseStamped
        current_distance= speed*(t1-t0)
    #After the loop, stops the robot
    vel_msg.linear.x = 0
    #Force the robot to stop
    velocity_publisher.publish(vel_msg)
    return


def rotation(velocity_publisher, vel_msg, angular_speed, angle):

    relative_angle = angle*2*PI/360

    # CCW
    vel_msg.angular.z = abs(angular_speed)

    # Setting the current time for distance calculus
    t0 = rospy.Time.now().to_sec()
    current_angle = 0

    while(current_angle < relative_angle):
        velocity_publisher.publish(vel_msg)
        t1 = rospy.Time.now().to_sec()
        current_angle = angular_speed*(t1-t0)


    #Forcing our robot to stop
    vel_msg.angular.z = 0
    velocity_publisher.publish(vel_msg)
    return


def finish():
    print
    print "node has finished"
    return


if __name__ == '__main__':
    try:
        #Testing our function
        move()
    except rospy.ROSInterruptException: pass

