#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/ivan/Documents/hexagon/meyer/build/devel:$CMAKE_PREFIX_PATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/ivan/Documents/hexagon/meyer/build/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/ivan/Documents/hexagon/meyer:/opt/ros/kinetic/share"