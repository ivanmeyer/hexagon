# README #

## Developer ##

Ivan Meyer ............. ivan.meyer00@gmail.com

## Challenge ##

Create a ROS node that makes turtlesim draw an hexagon on the screen. 

## Optional features ##

* Make the speed of the turtle configurable.
* Extend the node to draw configurable arbitrary regular shapes, loaded as paths from a file.
* Propose and implement a unit test to validate the motion control code (which may affect the exact way the code is written).
* Add the ability to pause/resume the execution.

## Deliverable ##

All code must be provided in a public git repository, with a brief description and clear instructions on how to build and execute it. It should run on an Ubuntu 16.04 box, and be implemented using ROS Kinetic version. 


## Setup ##

In ~/.bashrc add:

    export ROS_PACKAGE_PATH=<PROJECT_PATH>/hexagon:$ROS_PACKAGE_PATH

where `<PROJECT_PATH>` is for example `~/Documents`

## Commands ##

The user needs to use this commands in a Terminal as follows:

    # Make the node
    rosmake meyer
    # Start ROS Master node
    roscore

In a new terminal:

    # Run turtlesim node
    rosrun turtlesim turtlesim_node

In a new terminal:

    # Run meyer node
    rosrun meyer move.py

## Execution example ##

    Ivan Meyer's node to draw a hexagon
    Input the lineal speed:5
    Input your speed (degrees/sec):30

    node has finished

